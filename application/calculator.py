def add_ints(a, b):
    if not isinstance(a, int) or not isinstance(b, int):
        raise ValueError("Invalid input types: {}, {}".format(a, b))

    return a + b
