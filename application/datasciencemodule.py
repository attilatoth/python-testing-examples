from __future__ import division


# reads a file to list
class DataReader(object):
    def __init__(self, input_stream):
        self.data_stream = input_stream

    # input file format:
    # name1-amount1
    # name2-amount2
    # name1-amount3
    def read_data(self):
        data = []
        for line in self.data_stream:
            values = line.split('-')
            self.__validate_values(values)
            values = [values[0], int(values[1])]

            data.append(values)
        return data

    def __validate_values(self, values):
        int(values[1])
        if len(values) != 2 or not values[0]:
            raise ValueError("Invalid input file format: {} {}".format(len(values), not values[0]))


#
class Aggregator(object):
    def __init__(self, reader):
        self.reader = reader

    # private method (dunder prefix) -> protected methods single underscore, just gentlemen's agreement
    # https://docs.python.org/2/tutorial/classes.html#private-variables-and-class-local-references
    def __filter_valid_data(self, income_map):
        return {k: v for k, v in income_map.items() if "invalid" not in k.lower()}

    def aggregate(self):
        input_data = self.reader.read_data()
        aggregated_income = {}
        for payment in input_data:
            name, amount = payment
            aggregated_income[name] = aggregated_income.get(name, 0) + amount

        return self.__filter_valid_data(aggregated_income)


class MLModel(object):
    def __init__(self, input_data):
        self.data = input_data

    # apply model
    def calculate_ratios(self):
        s = sum(self.data.values())

        return {entry[0]: entry[1] / s for entry in self.data.items()}


class Reporter(object):
    def __init__(self, input_data):
        self.data = input_data

    def __convert_input_to_sorted_list(self):
        l = self.data.items()
        self.sorted_list = sorted(l, key=lambda tpl: tpl[0])

    def generate_report_data(self):
        self.__convert_input_to_sorted_list()
        comma_delimited_list = ["{}\t{}".format(tpl[0], tpl[1]) for tpl in self.sorted_list]
        comma_delimited_list.insert(0, "#name\tratio")
        return "\n".join(comma_delimited_list)


class Application(object):
    def __init__(self, bank_report_stream):
        self.input_reader = DataReader(bank_report_stream)
        self.aggregator = Aggregator(self.input_reader)

    def run(self):
        try:
            income_map = self.aggregator.aggregate()

            # should this part of the code be run in case of exception ?
            self.model = MLModel(income_map)
            ratio_map = self.model.calculate_ratios()

            self.reporter = Reporter(ratio_map)
            report_string = self.reporter.generate_report_data()
            print report_string
        except ValueError as ve:
            error_msg = "There was a problem while reading input file. Error: {}".format(ve)
            print(error_msg)


if __name__ == '__main__':

    bank_report_file_name = "bank_input.txt"
    try:
        input_stream = open(bank_report_file_name, 'r')
        app = Application(input_stream)
        app.run()
    except IOError as ioe:
        error_msg = "IOError: {}".format(ioe)
        print(error_msg)
