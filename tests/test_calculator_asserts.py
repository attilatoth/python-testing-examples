from unittest import TestCase

from application.calculator import add_ints


class TestCalculator(TestCase):
    def test_add_working_properly_with_greater(self):
        # given
        a, b = 0, 4

        # when
        actual = add_ints(a, b)

        # then
        self.assertGreater(actual, 0)

    def test_add_working_properly_with_assert_true(self):
        # given
        a, b = 3, 4

        # when
        actual = add_ints(a, b)

        # then
        self.assertTrue(actual == 7)

    def test_add_working_properly(self):
        # given
        a, b = 3, 4
        expected = a + b

        # when
        actual = add_ints(a, b)

        # then
        self.assertEqual(expected, actual)

    def test_add_working_properly_other(self):
        # given
        a, b = 3, 4

        # when
        actual = add_ints(a, b)

        # then
        self.assertEqual(actual, 7)

    def test_add_working_properly_shortest(self):
        # given

        # when
        actual = add_ints(3, 4)

        # then
        self.assertEqual(actual, 7)

    def test_add_with_invalid_input_float(self):
        # given
        a, b = 3, 4.5

        # when
        # with self.assertRaises(ValueError):
        with self.assertRaisesRegexp(ValueError, "^Invalid input types: \d, \d\.\d$"):
            add_ints(a, b)

    def test_add_with_invalid_input_string(self):
        # given
        a, b = 3, "4R5"

        # when
        with self.assertRaisesRegexp(ValueError, "^Invalid input types: \d, \d.\d$"):
            add_ints(a, b)
