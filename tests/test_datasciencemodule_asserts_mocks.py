import unittest
import StringIO

from mock import patch

from application.datasciencemodule import Aggregator
from application.datasciencemodule import DataReader


class TestDataReader(unittest.TestCase):
    # by using input stream, there is no need for testing missing file

    # def test_missing_file(self):
    #     # given
    #     file_name = 'FAKE_FILE_NAME'
    #
    #     # when
    #     # Specify error msg with regexp
    #     # with self.assertRaisesRegexp(IOError, '...'):
    #     with self.assertRaises(IOError):
    #         reader = DataReader(file_name)
    #         reader.read_data()

    # by using input stream, there is not need to mock the built-in open function
    def test_invalid_input(self):
        # given
        input_stream = StringIO.StringIO('andrea-adsf\nbela-2\n')
        reader = DataReader(input_stream)

        # when
        with self.assertRaisesRegexp(ValueError, "invalid literal"):
            reader.read_data()

    def test_valid_input(self):
        # given
        input_stream = StringIO.StringIO('andrea-1\nbela-2\n')
        reader = DataReader(input_stream)

        # when
        actual = reader.read_data()

        # then
        self.assertEqual(actual, [['andrea', 1], ['bela', 2]])

    def test_empty_input(self):
        pass


# helper function
def create_patch(test_class, class_name):
    patcher = patch(class_name)
    mock_obj = patcher.start()
    test_class.addCleanup(patcher.stop)
    return mock_obj


class AggregatorTest(unittest.TestCase):
    def test_aggregate(self):
        # given
        reader_mock = create_patch(self, 'application.datasciencemodule.DataReader')
        reader_mock.read_data.return_value = [['andrea', 1], ['bela', 2]]

        # reader_mock = DataReader('dummy filename, because it is not going to be used')
        aggregator = Aggregator(reader_mock)

        # when
        actual = aggregator.aggregate()

        # then
        self.assertEqual(actual, {'andrea': 1, 'bela': 2})

    def test_aggregate_with_block(self):
        # given
        with patch('application.datasciencemodule.DataReader') as reader_mock:
            reader_mock.read_data.return_value = [['andrea', 1], ['bela', 2]]
            # reader_mock = DataReader('dummy filename, because it is not going to be used')
            aggregator = Aggregator(reader_mock)

            # when
            actual = aggregator.aggregate()

            # then
            self.assertEqual(actual, {'andrea': 1, 'bela': 2})

    def test_aggregate_with_multiple_income_for_a_person(self):
        # given
        with patch('application.datasciencemodule.DataReader') as reader_mock:
            reader_mock.read_data.return_value = [['andrea', 1], ['bela', 2], ['andrea', 99]]
            # reader_mock = DataReader('dummy filename, because it is not going to be used')
            aggregator = Aggregator(reader_mock)

            # when
            actual = aggregator.aggregate()

            # then
            self.assertEqual(actual, {'andrea': 100, 'bela': 2})

    def test_aggregate_filtering_valid_data(self):
        pass


class TestMLModel(unittest.TestCase):
    pass


class TestReporter(unittest.TestCase):
    pass


class TestApplication(unittest.TestCase):
    pass
